/**
 * Created by amen on 8/16/17.
 */
public class Hero {

    private String name;
    private int hp;

    private IStrategy strategiaWalki;

    public Hero(String name, int hp) {
        this.name = name;
        this.hp = hp;
    }

    public void setStrategiaWalki(IStrategy strategiaWalki) {
        this.strategiaWalki = strategiaWalki;
    }

    public void fightUsingStrategy(){
        strategiaWalki.fight();
    }

//
//    public void fightWithDragon(){
//        System.out.println("Walcze mieczem");
//    }
//
//
//    public void fightWithDragonUsingArrows(){
//        System.out.println("Walcze szczalami");
//    }
}
